package conveyor.service.controller;

import conveyor.api.dto.CreditDTO;
import conveyor.api.dto.LoanApplicationRequestDTO;
import conveyor.api.dto.LoanOfferDTO;
import conveyor.api.dto.ScoringDataDTO;
import conveyor.service.exception.RefusalOfLoanException;
import conveyor.service.service.CreditService;
import conveyor.service.service.LoanOfferService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/conveyor")
@AllArgsConstructor
public class ConveyorController {

    private final LoanOfferService loanOfferService;
    private final CreditService creditService;

    @Operation(summary = "Get Loan Offers LoanOfferDTO List")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", content =
            @Content(schema = @Schema(implementation = LoanOfferDTO.class)), description = "LoanOfferDTO List"),
            @ApiResponse(responseCode = "400", description = "Invalid LoanApplicationRequestDTO")
    })
    @PostMapping("/offers")
    public List<LoanOfferDTO> loanOffers(@RequestBody LoanApplicationRequestDTO loanApplicationRequestDTO) {
        return loanOfferService.getLoanOffersCredit(loanApplicationRequestDTO);
    }

    @Operation(summary = "Get Calculation Credit CreditDTO")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", content =
            @Content(schema = @Schema(implementation = CreditDTO.class)), description = "CreditDTO"),
            @ApiResponse(responseCode = "400", description = "Invalid ScoringDataDTO")
    })
    @PostMapping("/calculation")
    public CreditDTO calculationCredit(@RequestBody ScoringDataDTO scoringDataDTO) throws RefusalOfLoanException {
        return creditService.getCalculationCredit(scoringDataDTO);
    }
}