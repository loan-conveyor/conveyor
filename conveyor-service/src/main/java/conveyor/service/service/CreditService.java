package conveyor.service.service;

import conveyor.api.dto.CreditDTO;
import conveyor.api.dto.ScoringDataDTO;
import conveyor.service.exception.RefusalOfLoanException;

public interface CreditService {
    CreditDTO getCalculationCredit(ScoringDataDTO scoringDataDTO) throws RefusalOfLoanException;
}