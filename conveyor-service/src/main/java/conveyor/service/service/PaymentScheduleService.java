package conveyor.service.service;

import conveyor.api.dto.PaymentScheduleElementDTO;

import java.math.BigDecimal;
import java.util.List;

public interface PaymentScheduleService {
    List<PaymentScheduleElementDTO> calculatePaymentScheduleElementList(final Integer term,
                                                                        final BigDecimal totalAmount,
                                                                        final BigDecimal rate,
                                                                        final BigDecimal monthlyPayment);
}
