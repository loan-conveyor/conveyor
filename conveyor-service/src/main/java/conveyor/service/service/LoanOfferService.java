package conveyor.service.service;

import conveyor.api.dto.LoanApplicationRequestDTO;
import conveyor.api.dto.LoanOfferDTO;

import java.math.BigDecimal;
import java.util.List;

public interface LoanOfferService {
    List<LoanOfferDTO> getLoanOffersCredit(LoanApplicationRequestDTO loanApplicationRequestDTO);

    BigDecimal calculatedTotalAmount(BigDecimal amount, Integer term, BigDecimal newRate, Boolean isInsuranceEnabled);

    BigDecimal correctingTotalAmount(BigDecimal paymentInMouth, Integer term);

    BigDecimal calculatedPaymentInMouth(Integer term, BigDecimal totalAmount);

    BigDecimal calculatedRareByInsuranceEnabledAndSalaryClient(boolean isInsuranceEnabled, boolean isSalaryClient, BigDecimal rate);
}
