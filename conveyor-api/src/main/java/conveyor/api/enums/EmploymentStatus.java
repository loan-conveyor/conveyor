package conveyor.api.enums;

import com.fasterxml.jackson.annotation.JsonProperty;

public enum EmploymentStatus {
    @JsonProperty("unemployed")
    UNEMPLOYED,
    @JsonProperty("selfEmployed")
    SELF_EMPLOYED,
    @JsonProperty("employed")
    EMPLOYED,
    @JsonProperty("businessOwner")
    BUSINESS_OWNER
}