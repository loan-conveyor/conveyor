package conveyor.api.enums;

import com.fasterxml.jackson.annotation.JsonProperty;

public enum Position {
    @JsonProperty("worker")
    WORKER,
    @JsonProperty("midManger")
    MID_MANAGER,
    @JsonProperty("topManager")
    TOP_MANAGER,
    @JsonProperty("owner")
    OWNER
}