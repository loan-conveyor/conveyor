package conveyor.api.enums;

import com.fasterxml.jackson.annotation.JsonProperty;

public enum Gender {
    @JsonProperty("male")
    MALE,
    @JsonProperty("female")
    FEMALE,

    @JsonProperty("nonBinary")
    NON_BINARY
}