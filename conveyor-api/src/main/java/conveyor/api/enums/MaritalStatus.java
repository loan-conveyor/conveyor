package conveyor.api.enums;

import com.fasterxml.jackson.annotation.JsonProperty;

public enum MaritalStatus {
    @JsonProperty("married")
    MARRIED,
    @JsonProperty("divorced")
    DIVORCED,
    @JsonProperty("single")
    SINGLE,
    @JsonProperty("widowWidower")
    WIDOW_WIDOWER
}